<?php

namespace Drupal\traffic_light_rating\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Field widget "traffic_light_rating_default".
 *
 * @FieldWidget(
 *   id = "traffic_light_rating_default",
 *   label = @Translation("Traffic Light Rating Default"),
 *   field_types = {
 *     "traffic_light_rating",
 *   }
 * )
 */
class TrafficLightRatingDefaultWidget extends WidgetBase implements WidgetInterface {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item =& $items[$delta];

    $element += [
      '#type' => 'details',
      '#open' => FALSE,
    ];

    $element['value'] = [
      '#type' => 'number',
      '#title' => t('Value'),
      '#description' => t('Numeric value in grammes.'),
      '#default_value' => $item->value ?? '',
      '#min' => 0,
      '#max' => 100,
      '#step' => 0.1,
      '#required' => $item->getFieldDefinition()->isRequired() ?? FALSE,
      '#field_suffix' => 'g',
      '#attributes' => [
        'style' => ['width: 70px;'], // Adding this as 'size' has no effect.
      ],
    ];

    $element['status'] = [
      '#type' => 'select',
      '#title' => t('Traffic light status'),
      '#options' => ['low' => 'Low', 'med' => 'Medium', 'high' => 'High'] ?? [],
      '#description' => t('Status: low, medium, or high.'),
      '#default_value' => $item->status ?? '',
      '#required' => $item->getFieldDefinition()->isRequired() ?? FALSE,
    ];

    return $element;
  }

}
