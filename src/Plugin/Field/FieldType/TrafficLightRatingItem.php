<?php

namespace Drupal\traffic_light_rating\Plugin\Field\FieldType;

use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;

/**
 * Field type "traffic_light_rating".
 *
 * @FieldType(
 *   id = "traffic_light_rating",
 *   label = @Translation("Traffic Light Rating"),
 *   description = @Translation("Displays nutrition information (fat, saturated fats, sugar and salt) for food or drink products."),
 *   default_widget = "traffic_light_rating_default",
 *   default_formatter = "traffic_light_rating_default",
 * )
 */
class TrafficLightRatingItem extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $output = [];

    $output['columns']['value'] = [
      'type' => 'varchar',
      'length' => 5,
    ];

    $output['columns']['status'] = [
      'type' => 'varchar',
      'length' => 10,
    ];

    return $output;

  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {

    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Value'))
      ->setRequired(FALSE);

    $properties['status'] = DataDefinition::create('string')
      ->setLabel(t('Status'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $is_dirty = FALSE;

    foreach ($this->getValue() as $input) {
      if (isset($input) && !empty($input)) {
        $is_dirty = TRUE;
      }
    }

    return !$is_dirty;
  }

}
