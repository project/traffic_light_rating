<?php

namespace Drupal\traffic_light_rating\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Field formatter "traffic_light_rating_default".
 *
 * @FieldFormatter(
 *   id = "traffic_light_rating_default",
 *   label = @Translation("Traffic Light Rating Default"),
 *   field_types = {
 *     "traffic_light_rating",
 *   }
 * )
 */
class TrafficLightRatingDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $output = [];
    $build = [];

    // Abbreviated terms.
    $abv = [
      'Saturated fat' => 'Sat',
      'Fat' => 'Fat',
      'Sugar' => 'Sugar',
      'Salt' => 'Salt',
    ];

    // Iterate each field and generate the render array.
    foreach ($items as $delta => $item) {
      if (!empty($item->value) && !empty($item->status)) {
        $build['traffic_light'] = [
          '#theme' => 'traffic_light_rating',
          '#name' => $item->getFieldDefinition()->getLabel(),
          '#abv' => $abv[$item->getFieldDefinition()->getLabel()],
          '#value' => $item->value,
          '#status' => $item->status,
        ];
      }

      $output[$delta] = $build;
    }

    // Add the CSS for the traffic light.
    $output['#attached']['library'][] = 'traffic_light_rating/field-display';

    return $output;
  }

}
